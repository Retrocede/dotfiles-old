" Plugins
call plug#begin("~/.vim/plugged")

" Languages
Plug 'sheerun/vim-polyglot'
Plug 'othree/html5.vim'

" VueJS
Plug 'posva/vim-vue'

" NerdTree
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }

" Git
"Plug 'tpope/vim-fugitive'

" Airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Theme
"Plug 'morhetz/gruvbox'
Plug 'gruvbox-community/gruvbox'

" Autoread
Plug 'djoshea/vim-autoread'

" Autocomplete
"Plug 'neoclide/coc.nvim', {'tag': '*', 'do': './install.sh'}
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"Plug 'ludovicchabant/vim-gutentags'
"Plug 'neomake/neomake'

" Elixir
Plug 'slashmili/alchemist.vim'
Plug 'mhinz/vim-mix-format' " auto formats on save

" Phoenix
Plug 'c-brenn/phoenix.vim'
Plug 'tpope/vim-projectionist'

call plug#end()
" End Plugins

" General Config
set number
set nocompatible
set cursorline
set modelines=0
set laststatus=2
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set title
filetype plugin indent on

" use ; and : interchangeably (yay no shift)
map ; :

" Enable clipboard for nvim
set clipboard=unnamedplus

" Set leader to comma
let mapleader = ","
map <leader>nt :NERDTreeToggle <CR>

" Theming
set laststatus=2
let g:airline_powerline_fonts=1
syntax enable
set background=dark
let g:gruvbox_italic=1
let g:gruvbox_italicize_strings=1
colorscheme gruvbox
let g:airline_theme="gruvbox"

" CTRL-navkeys to switch panes
map <C-J> <C-W>j
map <C-K> <C-W>k
map <C-L> <C-W>l
map <C-H> <C-W>h

" split mappings
nmap <silent> <leader>sv :vsp<CR>
nmap <silent> <leader>sh :sp<CR>
set splitbelow
set splitright

" use Esc to get out of terminal mode
:tnoremap <Esc> <C-\><C-n>

" Quickly edit/reload the vimrc file
"nmap <silent> <leader>ev :e $MYVIMRC
"nmap <silent> <leader>sv :so $MYVIMRC

" Tab Shortcuts
nmap <silent> <leader>to :tabnew<CR>
nmap <silent> <leader>tc :tabclose<CR>
nmap <silent> <leader>tr :tabm +1<CR>
nmap <silent> <leader>tl :tabm -1<CR>


" Lookup TODO comment
"nmap <silent> <leader>t :noautocmd vimgrep /TODO/j **/*<CR>:cw<CR>

" Run tests for current elixir directory
" nmap <silent> <leader>t :!mix test<CR>

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Configure to show whitespace characters
":set listchars=eol:⏎,tab:␉·,trail:␠,extends:>,precedes:<,space:␣
":set list

" move swap files out of working directory
set dir=~/.vim/tmp//

" Syntax customization for various extensions
autocmd BufNewFile,BufRead *.es6 set syntax=javascript

" Remove trailing whitespace on write
autocmd BufWritePre * :%s/\s\+$//e

" JSDOC key
nmap <silent> <C-d> ?function<cr>:noh<cr><Plug>(jsdoc)

" Mapping to disable highlighting until next search
nnoremap <silent> <F3> :noh<cr>

" mouse fix for iterm2
"set mouse=a
"if has("mouse_sgr")
"    set ttymouse=sgr
"else
"    set ttymouse=xterm2
"end

" JS template strings
let g:javascript_plugin_flow = 1
let g:jsx_ext_required = 0

" highlight 100th col
set colorcolumn=100

" auto format elixir files
let g:mix_format_on_save = 1

" Deoplete config
let g:deoplete#enable_at_startup = 1

" use tab for completion
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

" set tag dir
let g:gutentags_cache_dir='~/.tag_cache'
"let g:gutentags_trace=1
" disable alchemist tag (using gutentags instead)
"let g:alchemist_tag_disable = 1
" run neomake when I save a buffer
"autocmd! BufWritePost * Neomake

